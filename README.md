# Проекты выполненные в рамках курса Специалист по Data Science в Яндекс Практикум
В этом репозитории размещены учебные проекты.

Каждый проект снабжен:

* описанием проекта;
* сферой деятельности в которой проект может быть применен;
* изученными инструментами и полученными навыками;
* ключевыми словами.

### Сравнение пользователей Яндекс.Музыки в Москве и Санкт-Петербурге
[Каталог и описание проект №1](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/1.big_city_music)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/1.big_city_music/big_city_music_for_gitlab.ipynb)

В проекте мы проверили гипотезы о том, похожи или различны вкусы пользователей Яндекс Музыки в Москве и Санкт-Петербурге.

Используемые инструменты:

python, pandas, numpy.


### Исследование надежности заемщиков банка
[Каталог и описание проект №2](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/2.bank_borrower_reliability_study)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/2.bank_borrower_reliability_study/bank_borrower_reliability_study_for_gitlab.ipynb)

В проекте построена модель кредитного скоринга — специальной системы, которая оценивает способность потенциального заёмщика вернуть кредит банку.

Используемые инструменты:

python, pandas, numpy.


### Рекомендация тарифов телеком оператора
[Каталог и описание проект №4](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/4.recommendation_of_telecom_operator_tariffs)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/4.recommendation_of_telecom_operator_tariffs/recommendation_of_telecom_operator_tariffs_for_gitlab.ipynb)

В проекте необходимо проанализировать поведение клиентов и сделать вывод — какой тариф лучше.

Для этого мы сделали предварительный анализ тарифов на небольшой выборке клиентов.

В нашем распоряжении данные 500 пользователей оператора: кто они, откуда, каким тарифом пользуются, сколько звонков и сообщений каждый отправил за 2018 год.

Используемые инструменты:

python, pandas, numpy, matplotlib, seaborn, scipy.


### Анализ продаж компьютерных игр интернет-магазина
[Каталог и описание проект №5](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/5.analysis_of_computer_games)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/5.analysis_of_computer_games/Analysis_of_the_sale_of_computer_games_online_store_for_gitlab.ipynb)

В проекте необходимо выявить определяющие успешность игры закономерности. Это позволит сделать ставку на потенциально популярный продукт и спланировать рекламные кампании.

Используемые инструменты:

python, pandas, numpy, matplotlib, seaborn, scipy.

### Классификация клиентов телеком компании
[Каталог и описание проект №6](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/6.recommendation_of_telecom_operator_tariffs_ML)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/6.recommendation_of_telecom_operator_tariffs_ML/recommendation_of_telecom_operator_tariffs_ML_for_gitlab.ipynb)

В проекте необходимо построить модель машинного обучения для задачи классификации, чтоб она помогла подобрать оптимальный тариф.

Используемые инструменты:

python, pandas, numpy, sklearn.


### Отток банковских клиентов
[Каталог и описание проект №7](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/7.bank_customer_churn)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/7.bank_customer_churn/7.the_outflow_of_bank_customers_for_gitlab.ipynb)

В проекте необходимо построить модель машинного обучения для задачи классификации, чтоб она прогнозировала уход клиентов из банка.

Используемые инструменты:

python, pandas, numpy, sklearn, matplotlib.


### Выбор локации для нефтяной скважины
[Каталог и описание проект №8](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/8.locatiom_for_oil)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/8.locatiom_for_oil/Choosing_a_location_for_an_oil_well_for_gitlab.ipynb)

В проекте необходимо построить регрессионную модель машинного обучения для определения региона, где добыча принесёт наибольшую прибыль.

Используемые инструменты:

Python, Pandas, Scikit-learn, Bootstrap.


### Исследование технологического процесса очистки золота

[Каталог и описание проект №9](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/9.gold_recovery)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/9.gold_recovery/recovery_of_gold_from_ore_for_gitlab.ipynb)

В проекте необходимо построить регрессионную модель машинного обучения, которая может спрогнозировать концентрацию золота при проведении процесса её очистки из золотосодержащей руды.

Используемые инструменты:

Python, Pandas, Matplotlib, NumPy, Scikit-learn.


### Защита персональных данных клиентов
[Каталог и описание проект №10](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/tree/main/10.insurance)

[Файл проекта](https://gitlab.com/anm78_jp/yandex-practicum-projects/-/blob/main/10.insurance/protection_of_personal_data_of_clients_for_gitlab.ipynb)

В проекте необходимо построить модель машинного обучения, и создать алгоритм для защиты данных клиентов страховой компании.

Используемые инструменты:

Python, Pandas, NumPy, Scikit-learn.