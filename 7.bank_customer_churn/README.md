# Outflow of bank customers

# Отток банковских клиентов

### Спринт
Обучение с учителем


### Сферы деятельности компаний
Бизнес, Инвестиции.


### Направление деятельности
Банковская сфера, Кредитование, анализ данных, машинное обучение. (Banking, Lending, data analysis, machine learning.)


### Навыки и инструменты

Навыки полученые в рамках изучения данного спринта/темы, а также инструменты, которые использовались при решении проекта:

Машинное обучение, Классификация, Python, Pandas, Numpy, Matplotlib,Scikit-learn.


### Описание проекта
Из Банка стали уходить клиенты. Каждый месяц. Немного, но заметно.

Банковские маркетологи посчитали: сохранять текущих клиентов дешевле, чем привлекать новых.


Цель проекта:

Нужно спрогнозировать, уйдёт клиент из банка в ближайшее время или нет.


Задачи проекта:

Необходимо построить модель машинного обучение с предельно большим значением F1-меры. Чтобы проект стал успешным, нужно довести метрику до 0.59. Проверить F1-меру на тестовой выборке.

Дополнительно измерить AUC-ROC, сравнить её значение с F1-мерой.


Вывод проекта:

* клиентами банка являются жители стран France, Germany, Spain;
* женщин среди клиентов, меньше чем мужчин почти на 1000;
* возраст клиентов от 18 и до 92 лет;
* подавляющее большинство клиентов используют 1 или 2 банковских продукта;
* продолжительность лет использования банковских продуктов клиентами варьируется от 0 до 10;


Что сделано:

Изучили и подготовили данные. Выяснили, что в выборке 20% клиентов тех, кто ушел из банка. В представленных данных классы оказались не сбалансированными. Были проведены тестирования направленные на балансировку классов выборки. Далее была найдена лучшая модель и подобраны гиперпараметры, позволившие получить F-меру больше 0.59.


Ключевые слова проекта

классификация, подбор гиперпараметров, выбор модели МО.