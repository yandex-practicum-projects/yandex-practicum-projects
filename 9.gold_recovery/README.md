# Recovery of gold from ore

# Исследование технологического процесса очистки золота

### Спринт
Сборный проект №2 по Data Science.


### Сферы деятельности компаний
Промышленность


### Направление деятельности
Машинное обучение, Аналитик (универсал). (Machine learning, Analyst (generalist)).


### Навыки и инструменты

Навыки полученые в рамках изучения данного спринта/темы, а также инструменты, которые использовались при решении проекта:

Python, Pandas, Matplotlib, NumPy, Scikit-learn, исследовательский анализ данных.


### Описание проекта

В данном проекте необходимо спрогнозировать концентрацию золота при проведении процесса её очистки из золотосодержащей руды.

Цель проекта:

Создать модель машинного обучения, которая должна предсказать коэффициент восстановления золота из золотосодержащей руды.

Задачи проекта:

* Подготовить данные;
* Провести исследовательский анализ данных;
* Построить и обучить модель.

Вывод проекта:

В ходе работы данные были осмотрены, проанализированы необходимые параметры, данные подготовили для обучения. Удалось создать модель машинного обучения с подобранными гиперпараметрами, которая предсказала отличный результат.

Что сделано:

* Осмотрели и подготовили данные.
* Проверили, что эффективность обогащения рассчитана правильно.
* Проанализировали недоступные в тестовой выборке признаки.
* Провели предобработку данных.
* Сравнили распределения размеров гранул сырья в обучающем и тестовом датасетах.
* Исследовали суммарную концентрацию всех веществ на разных стадиях очистки.
* Создали, обучили модели, проверили лучшую модель на тестовых данных.

Ключевые слова проекта:

анализ данных, регрессия, кастомные метрики.